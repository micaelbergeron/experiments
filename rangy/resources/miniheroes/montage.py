import os
import glob
import json
from subprocess import call

base_path = "32x32/"
def path(path):
    return base_path + path;

sprite_size = 32

sprite_path = path('sprite.png')
meta_path = 'sprite.json'

entries = list()
last_entry_index = 0;
for image in glob.glob(path('*.png')):
    entry = { 'name': image.replace("\\", "/") }
    entry['x'] = last_entry_index * sprite_size
    entry['y'] = 0
    entries.append(entry)
    last_entry_index += 1

print 
with open(meta_path, 'w+') as f_meta:
    json.dump(entries, f_meta,
            sort_keys=True, indent=4)


DEBUG=true;

var sqrt_2 = 1.41;
var miniheroes_meta = null;
var mousepos = [0, 0];

function init() {
    var fetching_miniheroes = fetch_miniheroes();
    var canvas = initCanvas("canvas", 1920, 1080);
    canvas.addEventListener('mousemove', function(event) {
        var xy = canvas.leftTopScreen();
        mousepos = [event.clientX - xy[0], event.clientY - xy[1]];
    });
    update();

    fetching_miniheroes.done(function(meta) {
        miniheroes_meta = meta;
    });
}
function setBackground(ctx, image) {
    ctx.drawImage(image, 0, 0);
}
function drawRadius(ctx, radius)
{
    ctx.translate(canvas.width/2 - 53, canvas.height/2 - 40);

    ctx.scale(1.10,0.95);
    //ctx.rotate(2*sqrt_2*Math.PI);

    ctx.beginPath();
    ctx.arc(0, 0, radius, 0, 2*Math.PI);

    ctx.fillStyle = "rgba(255,255,150,0.2)"
        ctx.strokeStyle = "white";
    ctx.fill();
    ctx.stroke();
}
function dota2UnitToPx(unit) {
    // empirically I found that 45px=125du
    return unit*45/125;
}

var startedAt = Date.now();
var lastTime = startedAt;
var lastDeltaTime = 0;
function deltaTime(top) {
    if (!top) {
        return lastDeltaTime;
    }

    var now = Date.now();
    lastDeltaTime = now - lastTime;
    lastTime = now;

    return lastDeltaTime; 
}
// main loop
function update() {
    var canvas = document.getElementById('canvas');
    var dt = 0;
    //- do stuff
    if (dt = deltaTime(true)) {
        draw(canvas); 
    }
    
    window.requestAnimationFrame(update);
}
function draw(canvas) {
    // reset
    var ctx = canvas.getContext('2d');
    ctx.setTransform(1,0,0,1,0,0);

    var background = document.images[0];
    if (background)
        //setBackground(ctx, background);

    canvas.width = canvas.width;
    if (DEBUG) {
        drawFPS(ctx,0,10);
    }
    drawReferenceElements(ctx);
    drawRadius(ctx, 400);    
}
function drawFPS(ctx, x, y) {
    var fps = 1000/deltaTime();
    ctx.save();

    ctx.fillText(String(fps),x,y); 

    ctx.restore();
}
function drawReferenceElements(ctx) {
    // a 1200 reference line will do for a blink daggerz
    var width = 1200;
    var top = 100;
    var left = 50;

    ctx.save();
    ctx.strokeStyle="black";

    ctx.beginPath();
    ctx.moveTo(left, top);
    ctx.lineTo(dota2UnitToPx(left+width), top);
    ctx.stroke();

    var beastmaster = {
        icon: '32x32/beastmaster.png',
        x: 224, y:0
    };
    var image = document.images[1];

    ctx.drawImage(image, 
            beastmaster.x, beastmaster.y,
            32, 32,
            mousepos[0] - 16, mousepos[1] - 16,
            32, 32);

    ctx.restore();
}
